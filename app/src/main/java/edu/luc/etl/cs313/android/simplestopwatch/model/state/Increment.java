package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class Increment implements StopwatchState {

    public Increment(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;
    int tickcount = 0;
    @Override
    public void onStartStop() {
        tickcount = 0;
        sm.actionInc();
        sm.toIncrement();
    }

    @Override
    public void onTick() {
        if(tickcount==3){
            sm.actionRingAlarm();
            sm.toRunningState();
        }
        else{++tickcount;}
    }//need counter that after 3 tics it will go into a different state (should go into running state) - done

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREMENT;
    }
}
