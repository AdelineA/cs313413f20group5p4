package edu.luc.etl.cs313.android.simplestopwatch.common;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

import java.io.IOException;

/**
 * A listener for UI update notifications.
 * This interface is typically implemented by the adapter, with the
 * notifications coming from the model.
 *
 * @author laufer
 */
public interface StopwatchUIUpdateListener {
    void updateTime(int timeValue);
    void updateState(int stateId);
    void playDefaultNotification();
}
