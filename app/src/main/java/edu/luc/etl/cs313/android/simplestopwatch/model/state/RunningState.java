package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionReset();//whatever action clears display is what we want
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        if (sm.countedDown())
        {
            sm.toAlarmingState();
        }
        else{
            sm.actionDec();//define in stopwatch default
            sm.toRunningState();//stays in running until we get to 0. once we get to 0, alarm goes off
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {return R.string.RUNNING;}
}
